Author: Yuhe Yuan
Instructions on how to build install and use this project

Environment Prerequisites:
SQL Express 2017 environment installed
Microsoft SQL Server Management Studio 2019 installed

How to use this project:
1.Start SQL Express 2017 service
2.Open Microsoft SQL Server Management Studio 2019
3.Open sql file "Build Plant Database.sql"
4.Click on "Execute" button on top left to run the sql file
5.The "Databases" folder in the Object Explorer window should have added a new Database named "Production"

Explaination of choice of License:
The MIT License(https://opensource.org/licenses/MIT) is permissive as the modified versions does not have to be issued under the same licensence,
files does not have to contain all information about changes, copyrights and patents and it does not prohibit the use
of copyright holder's name for promotion. The persmissiveness would allow me to combine codes with other open source libraries with only few restrictions.
The license is also simple unlike many other licenses with long and complex text.