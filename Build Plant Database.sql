--YUHE YUAN
PRINT '*** Question 1-4 ***'
PRINT ''
CREATE DATABASE Production
GO
USE Production
CREATE TABLE Customer(
ID INT IDENTITY PRIMARY KEY NOT NULL,
Name VARCHAR(100),
Phone VARCHAR(20),
Email VARCHAR(50)
)

GO
USE Production
CREATE TABLE Plant(
ID INT IDENTITY PRIMARY KEY NOT NULL,
Name VARCHAR(50),
)

CREATE TABLE VehicleType(
ID INT IDENTITY PRIMARY KEY NOT NULL,
Name VARCHAR(50),
)

GO
USE Production
CREATE TABLE PartType(
ID INT IDENTITY PRIMARY KEY NOT NULL,
Name VARCHAR(50)
)

GO
USE Production
CREATE TABLE Employee(
ID INT IDENTITY PRIMARY KEY NOT NULL,
FirstName VARCHAR(50), 
LastName VARCHAR(50),
PlantID INT,


constraint Employee_Part_FK 
foreign key (PlantID) references Plant(Id)
)


GO
USE Production
CREATE TABLE Address(
ID INT IDENTITY PRIMARY KEY NOT NULL, 
EmployeeID INT NOT NULL, 
Street VARCHAR(50), 
City VARCHAR(50), 
Province VARCHAR(20),
PostalCode VARCHAR(20), 
PhoneNumber VARCHAR(20), 

constraint Address_Employee_FK 
foreign key (EmployeeID) 
references Employee (ID) ON DELETE CASCADE
)

GO
USE Production
CREATE TABLE Part(
ID INT IDENTITY PRIMARY KEY NOT NULL,
Name VARCHAR(50),
PartTypeID INT NOT NULL,
VehicleTypeID INT NOT NULL

constraint Part_PartTypeID_FK 
foreign key (PartTypeID) references PartType (ID),

constraint Part_VehicleTypeID_FK 
foreign key (VehicleTypeID) references VehicleType (ID)
)

GO
USE Production
CREATE TABLE ProductionRecord(
ID INT IDENTITY PRIMARY KEY NOT NULL,
PartID INT,
CustomerID INT,
EmployeeID INT,
Time Int

constraint ProductionRecord_PartID_FK 
foreign key (PartID) references Part (ID),

constraint ProductionRecord_CustomerID_FK 
foreign key (CustomerID) references Customer (ID),

constraint ProductionRecord_EmployeeID_FK 
foreign key (EmployeeID) references Employee (ID)

)


PRINT '*** Question 5 ***'
PRINT ''
GO
USE Production
INSERT INTO Customer(Name,Phone,Email)
VALUES('Suzuki', '4166666666','suzuki@suzuki.ca'),
('BMW', '4168999888','bmw@bmw.ca');
select * from Customer

GO
USE Production
INSERT INTO PartType(Name)
VALUES('crankshaft'),('piston'),('exhaust')

select * from PartType

GO
USE Production
INSERT INTO Plant(Name)
VALUES('Waterloo'),('Guelph')

select * from Plant

GO
USE Production
INSERT INTO VehicleType(Name)
VALUES('Motorcycle'),('Car')

select * from VehicleType
GO
USE Production
INSERT INTO Part(Name,partTypeID,vehicleTypeID)
VALUES('MOTO001', 1,1),
('MOTO002',2,1),
('MOTO003',3,1),
('CAR001',2,2),
('CAR002',3,2),
('CAR003',1,2)

select * from Part
GO

USE Production
INSERT INTO Employee(FirstName,LastName,PlantID)
VALUES('Joe','Black',2),
('Frank','Gee',1),
('Mary','Gou',1),
('Gary','Dude',2),
('John','Ken',1)

select * from Employee

GO
USE Production
INSERT INTO Address(EmployeeID,Street,City,Province,PostalCode,PhoneNumber)
VALUES(1,'550 King St N','Waterloo','ON','N2L 5W6', '9051111111'),
(2,'17600 Yonge St', 'Newmarket', 'ON', 'L3Y 4Z1','4166663569'),
(3,'220 Yonge St','Toronto','ON','M5B 2H1','6476956863'),
(4,'1979  Wallbridge Loyalist Rd','Belleville','ON','K8N 1L9','6138496263'),
(5,'898 Beaver Creek','Thornhill','ON','L4J 1W2','9055308054')

select * from Address


GO
USE Production
INSERT INTO ProductionRecord(PartID,CustomerID,EmployeeID,Time)
VALUES
(1,1,1,45),
(1,1,2,40),
(2,1,1,20),
(4,1,3,15),
(4,2,3,20),
(2,2,3,10),
(1,2,4,65),
(5,2,4,110),
(3,1,5,45),
(6,1,5,25),
(6,2,2,45)	

select * from ProductionRecord

PRINT '*** Question 6a ***'
PRINT ''
GO
USE Production
select p.Name as 'Part Name', Time , e.FirstName + ' ' + e.LastName as 'Employee Name', Plant.Name as 'Plant Name'
from ProductionRecord pr, Part p, Employee e, Plant
where p.ID = pr.PartID and pr.CustomerID = 1 and e.ID = pr.EmployeeID and Plant.ID = e.PlantID

PRINT '*** Question 6b ***'
PRINT ''
GO
USE Production

select p.Name as 'Part #', pt.Name as 'Part Type', e.FirstName + ' '+e.LastName as 'Employee'
from ProductionRecord pr, Employee e, part p, PartType pt
where pr.EmployeeID in (select e.ID from Employee e where e.PlantID = 1) and 
e.ID = pr.EmployeeID and p.ID = pr.PartID and pt.ID = p.PartTypeID
order by 'Part #'
